class Post < ActiveRecord::Base
 has_many :comments, depedent: :destroy
 validates_presence_of :title
 validates_presence_of :body
end
